terraform {
  source = "${get_parent_terragrunt_dir()}/../modules/gitlab-baseline"
}

inputs = {
  enable_verification = false
}

include {
  path = find_in_parent_folders()
}
