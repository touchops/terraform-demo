terraform {
  source = "${get_parent_terragrunt_dir()}/../modules/gitlab-terraform"
}

inputs = {

  name        = "terraform"
  parent      = dependency.baseline.outputs.global_group
  description = "Distillery: Terraform"
  groups = {
    developer = "Distillery: Terraform"
    reporter =  "Distillery: Terraform reporters"
  }
}

dependency "baseline" {
  config_path = "../baseline"
}

include {
  path = find_in_parent_folders()
}
