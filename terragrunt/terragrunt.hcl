locals {
  gitlab = yamldecode(file("gitlab.yml"))
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<-EOF
    provider "gitlab" {
      base_url = "https://gitlab.com/api/v4/"
      insecure = false
      token = "${local.gitlab.token}"
    }
  EOF
}
