resource "gitlab_group" "terraform_distillery" {
  for_each = toset(var.groups)

  name             = join("_", [var.name, each.value])
  path             = join("_", [var.name, each.value])
  description      = "Distillery: Terraform development group"
  visibility_level = "private"
}

resource "gitlab_project_share_group" "terraform_distillery_develop" {
  for_each = toset([for g in gitlab_group.terraform_distillery: g.id])

  project_id   = gitlab_project.terraform.id
  group_id     = each.value
  access_level = "developer"
}
