provider "gitlab" {
  token    = var.token
  base_url = "https://gitlab.com/api/v4/"
  insecure = false
}
