resource "gitlab_group" "terraform" {
  for_each = var.groups

  parent_id = var.parent

  name             = join("_", [var.name, each.key])
  path             = join("_", [var.name, each.key])
  description      = "Distillery: ${each.value}"
  visibility_level = "private"
}

resource "gitlab_project_share_group" "terraform" {
  for_each = var.groups

  project_id   = gitlab_project.terraform.id
  group_id     = gitlab_group.terraform[each.key].id
  access_level = each.key
}
