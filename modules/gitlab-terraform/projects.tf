
resource "gitlab_project" "terraform" {

  namespace_id = var.parent

  name             = var.name
  description      = var.description
  visibility_level = "private"
}