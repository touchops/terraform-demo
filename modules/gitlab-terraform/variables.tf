variable "parent" {
  description = "ID of the parent group were projectand sub-groups are created"
  type        = number
}

variable "name" {
  description = "Name of Gitlab project for demonstration Terraform features"
  type        = string
}

variable "description" {
  description = "Gitlab project description"
  type        = string
}

variable "groups" {
  description = "Gitlab groups to share with project"
  type        = map(string)
  default     = {}
}
