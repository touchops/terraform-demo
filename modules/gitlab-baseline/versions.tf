terraform {
  required_providers {
    gitlab = ">= 2.9.0"
  }
  required_version = ">= 0.12.21"
}
