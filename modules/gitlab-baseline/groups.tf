resource "gitlab_group" "distillery" {
  name             = "distillery"
  path             = "distillery"
  description      = "Distillery Global"
  visibility_level = "private"
}
