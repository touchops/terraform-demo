
variable "token" {
  description = "API token to interact with GitLab"
  type        = string
}

variable "name" {
  description = "Name of Gitlab project for demonstration Terraform features"
  type        = string
}

variable "description" {
  description = "Gitlab project description"
  type        = string
}