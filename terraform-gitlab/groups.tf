resource "gitlab_group" "terraform_distillery" {
  name             = join("_", [var.name, "group"])
  path             = join("_", [var.name, "group"])
  description      = "Distillery: Terraform development group"
  visibility_level = "private"
}


resource "gitlab_project_share_group" "terraform_distillery_develop" {
  project_id   = gitlab_project.terraform.id
  group_id     = gitlab_group.terraform_distillery.id
  access_level = "developer"
}