
resource "gitlab_project" "terraform" {

  name        = var.name
  description = var.description

  visibility_level = "private"
}